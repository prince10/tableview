//
//  ViewController.m
//  tableview4
//
//  Created by Prince on 29/09/15.
//  Copyright (c) 2015 Prince. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    NSMutableArray *data;
    
}
@property (strong, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) IBOutlet UIButton *button;
@property (strong, nonatomic) IBOutlet UITableView *tableview1;



@end

@implementation ViewController
@synthesize tableview1;



- (void)viewDidLoad {
    

    data =[[NSMutableArray alloc]init];
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return data.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableview1 dequeueReusableCellWithIdentifier:@"remember"];
    cell.textLabel.text=data[indexPath.row];
    return cell;
}


-(IBAction) buttonPressed{

    [data addObject:_textField.text];
    [tableview1 reloadData];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
